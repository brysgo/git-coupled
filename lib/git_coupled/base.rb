module GitCoupled
  class Base
    class << self
      def related_to_file(file_related_to)
        ignore = Ignore.new
        RelatedFiles.group_by(file_related_to) do |group|
          added_modified_files=`git log --full-diff --name-status #{file_related_to} | egrep '[AM]\t'`
          files = added_modified_files.split(/\n|[AM]\t/).reject { |a| a == '' }
          files.each do |file|
            unless ignore.reject?(file)
              group.add_file(file)
            end
          end
        end.all
      end

      def file_coupling_percentage
        ignore = Ignore.new
        log_name_status=`git log --full-diff --name-status`
        split_by_commit = log_name_status.split(/commit [a-z0-9]{40}/)
        files = split_by_commit.map do |commit|
          commit.split(/\n/).select { |a|
            a.match(/^[AM]\t/)
          }.reject { |a| ignore.reject?(a) }.map do |line|
            line.gsub(/^[AM]\t/,'')
          end
        end.reject(&:empty?)
        files.each do |commit_files|
          commit_files.each do |file_to_group_by|
            RelatedFiles.group_by(file_to_group_by) do |group|
              commit_files.each do |file_to_add|
                group.add_file(file_to_add)
              end
            end
          end
        end
        #FIXME: don't leave this here
        a = Hash[RelatedFiles.groups.map do |grouped_file, group|
          one_quarter = group[grouped_file] / 4.0
          group.reject! do |file, count|
            file == grouped_file ||
            count < one_quarter ||
            count <= 3 ||
            !File.exists?(file)
          end

          [grouped_file, group]
        end]
        b = Hash[a.map do |grouped_file, group|
          new_group = group.select do |file, count|
            a[file].include? grouped_file
          end
          [grouped_file, new_group]
        end].reject { |k, v| v.empty? }
        <<-RESULT
        #{b.keys.length} / #{RelatedFiles.groups.length}
        #{b.sort_by { |k, v| -v.length }.map { |c|
        "#{c.last.length} #{c.first}"
        }.join("""
        """)}
        RESULT
      end
    end
  end
end
