module GitCoupled
  class Ignore
    def initialize
      @patterns = File.open(".coupledignore").map do |a|
        b = a.strip
        unless b.empty?
          Regexp.new(b)
        end
      end.compact
    end

    def reject?(file)
      @patterns.any? do |pattern|
        file.match(pattern)
      end
    end
  end
end
