module GitCoupled
  class CLI
    class << self
      def call(args=[])
        puts_or_less call_based_on_args(args)
      end

      private

      def puts_or_less(string)
        if string.length <= 20
          puts string
        else
          IO.popen("less", "w") { |f| f.puts string }
        end
      end

      def call_based_on_args(args)
        if args.length == 1 && File.file?(args[0])
          Base.related_to_file(args[0])
        else
          Base.file_coupling_percentage
        end
      end
    end
  end
end
