module GitCoupled
  class RelatedFiles < Hash
    attr_reader :related_to
    class << self
      attr_reader :groups
    end

    def self.group_by(related_to)
      @groups ||= {}
      @groups[related_to] ||= new(related_to)
      yield @groups[related_to]
      @groups[related_to]
    end

    def initialize(related_to)
      @related_to = related_to
    end

    def add_file(filename)
      self[filename] ||= 0
      self[filename] += 1
    end

    def all
      self.sort_by { |_, value| -value }
    end
  end
end
