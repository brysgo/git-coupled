require 'spec_helper'

module GitBycommit
  describe 'RelatedFiles' do
    describe '.group_by' do
      it 'creates a new related files object and yields it to a block' do
        new_related_files = double(:new_related_files)
        allow(RelatedFiles).to receive(:new).and_return(new_related_files)
        expect { |block|
          RelatedFiles.group_by('/the/file', &block)
        }.to yield_with_args(new_related_files)
      end

      it 'does a find or create with the file being grouped on' do
        raise 'test it'
      end
    end

    describe '#add_file' do
      subject(:group) { RelatedFiles.new('/the/file') }

      context 'when the file is not in the group' do
        it 'adds the file to the group' do
          expect(group.add_file('/a/file')).to eq 1
          expect(group.add_file('/a/different/file')).to eq 1
        end
      end

      context 'when the file is already in the group' do
        it 'increments the count of the file' do
          expect(group.add_file('/the/same/file')).to eq 1
          expect(group.add_file('/the/same/file')).to eq 2
        end
      end
    end

    describe '#all' do
      it 'returns [name, count] pairs of all the files sorted by count' do
        related_files = RelatedFiles.group_by('/a/file') do |g|
          g.add_file('/another/file')
          g.add_file('/the/same/file')
          g.add_file('/the/same/file')
        end

        expect(related_files.all).to eq [
          ['/the/same/file', 2],
          ['/another/file', 1]
        ]
      end
    end
  end
end
