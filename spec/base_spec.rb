require 'spec_helper'

describe GitCoupled::Base do
  describe '.related_to_file' do
    subject(:results) { GitCoupled::Base.related_to_file('Gemfile') }

    it 'the list returned is sorted by count' do
      results.inject(Float::INFINITY) do |last_result, result|
        expect(last_result).to be >= result[1]
        result[1]
      end
      expect(results.length).to be > 1
    end
  end

  describe '.file_coupling_percentage' do
    it 'works' do
      GitCoupled::Base.file_coupling_percentage
    end
  end
end
